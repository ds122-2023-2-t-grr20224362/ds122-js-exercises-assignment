
let SomaDeElementos = [1,2,3,4,5,6,7,8,9,10];
let soma = SomaDeElementos.reduce(function(soma, i) {
    return soma + i;
});
console.log(soma);
function soma(arr) {
  let acumulador = 0;

  for (let i = 0; i < arr.length; i++) {
    acumulador += arr[i];
  }

  return acumulador;
}

const array = [1, 2, 3, 4, 5, 6, 7, 8, 9];
const resultado = soma(array);
console.log(resultado);
