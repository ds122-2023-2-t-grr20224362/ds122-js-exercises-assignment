let array = [1, 2, 3, 4, 5];
let aux;

function inverteArray(array) {
  let i = 0;
  let j = array.length - 1;
  while (i < j) {
    aux = array[i];
    array[i] = array[j];
    array[j] = aux;
    i++;
    j--;
  }
  return array;
}
console.log(inverteArray(array));
