let linha1 = [" # # # #"];
let linha2 = ["# # # # "];
let espacoembranco = " ";
let i;

for (i = 1; i < 9; ++i) {
  if (i % 2 === 1) {
    espacoembranco += linha1;
  } else {
    espacoembranco += linha2;
  }

  espacoembranco += "\n";
}

console.log(espacoembranco);
console.log("\n");
